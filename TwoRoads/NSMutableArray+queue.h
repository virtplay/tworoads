//
//  NSMutableArray+queue.h
//  TwoRoads
//
//  Created by KARTHIK B S on 10/04/16.
//  Copyright © 2016 KARTHIK B S. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (queue)
- (void) enqueue: (id)item;
- (id) dequeue;
- (id) peek;
@end
