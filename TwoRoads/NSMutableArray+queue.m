//
//  NSMutableArray+queue.m
//  TwoRoads
//
//  Created by KARTHIK B S on 10/04/16.
//  Copyright © 2016 KARTHIK B S. All rights reserved.
//

#import "NSMutableArray+queue.h"

#define PLOT_POINTS 5

@implementation NSMutableArray (queue)
- (void) enqueue: (id)item {
    
    if (self.count==PLOT_POINTS) {
        [self peek]; // if queue is having PLOT_POINTS obj info remove one, before adding
    }
    [self addObject:item];
}

- (id) dequeue {
    id item = nil;
    if ([self count] != 0) {
        item = [self objectAtIndex:0];
        [self removeObjectAtIndex:0];
    }
    return item;
}

- (id) peek {
    id item = nil;
    if ([self count] != 0) {
        item = [self objectAtIndex:0];
    }
    return item;
}
@end
