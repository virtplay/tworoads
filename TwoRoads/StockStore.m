//
//  StockStore.m
//  TwoRoads
//
//  Created by KARTHIK B S on 09/04/16.
//  Copyright © 2016 KARTHIK B S. All rights reserved.
//

#import "StockStore.h"

@implementation StockStore
static NSMutableArray *infoarray;
static NSMutableArray *purchaseInfoarray;
static NSMutableArray *soldInfoarray;
static NSMutableArray *queuedArray;


+ (StockStore *)sharedInstance
{
    static StockStore *sharedInstance;
    
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
        infoarray = [[NSMutableArray alloc] init];
        purchaseInfoarray=[[NSMutableArray alloc] init];
        queuedArray=[[NSMutableArray alloc] init];
        soldInfoarray=[[NSMutableArray alloc] init];
    });
    
    return sharedInstance;
}

- (NSArray *)queuedPortfolioInfo{
    if (!queuedArray)
    {
        return nil;
    }
    return queuedArray;
}
-(void) addToQ:(StockInfo *) infoObj{
    [queuedArray addObject:infoObj];
}

- (NSArray *)soldPortfolioInfo{
    if (!soldInfoarray)
    {
        return nil;
    }
    return soldInfoarray;
}
-(void) addSoldStockInfo:(StockInfo *) infoObj{
    [soldInfoarray addObject:infoObj];
}

- (NSArray *)BoughtPortfolioInfo{
    if (!purchaseInfoarray)
    {
        return nil;
    }
    return purchaseInfoarray;

}

-(void) addPurchasedStockInfo:(StockInfo *) infoObj{
    [purchaseInfoarray addObject:infoObj];
}

-(void) addNewStockInfo:(StockInfo *) infoObj{
    [infoarray addObject:infoObj];
}

-(NSMutableArray *) getarray{
    return infoarray;
}


@end
