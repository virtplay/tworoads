//
//  ViewController.h
//  TwoRoads
//
//  Created by KARTHIK B S on 09/04/16.
//  Copyright © 2016 KARTHIK B S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"

@interface ViewController : UIViewController<NSStreamDelegate,CPTPlotDataSource>
@property (weak, nonatomic) IBOutlet UILabel *stockHeader;

@property (nonatomic, retain) NSInputStream *inputStream;
@property (nonatomic, retain) NSOutputStream *outputStream;

@property (weak, nonatomic) IBOutlet CPTGraphHostingView *chartview;

- (void) messageReceived:(NSString *)message;

@end

