//
//  ViewController.m
//  TwoRoads
//
//  Created by KARTHIK B S on 09/04/16.
//  Copyright © 2016 KARTHIK B S. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "ViewController.h"
#import "StockStore.h"
#import "StockInfo.h"
#import "NSMutableArray+queue.h"


#define PLOT_POINTS 5
@interface ViewController ()
@property NSString *initmsg;
@property NSTimer *timer;
@property CPTGraph *graph;
@property NSMutableArray *infoqueue;

@end

@implementation ViewController

@synthesize graph;
@synthesize inputStream,outputStream,initmsg,infoqueue,stockHeader;

NSString *  const CPDTickerSymbolBBP = @"Buy Price";
NSString *  const CPDTickerSymbolBSP = @"Sell Price";
static int counter;

- (void)viewDidLoad {
    [super viewDidLoad];
    initmsg=@"hello";
    counter=0;
    infoqueue=[[NSMutableArray alloc] init];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"stockblue.jpg"]]];
    
    stockHeader.layer.masksToBounds=YES;
    stockHeader.layer.cornerRadius=8;
    
    // Do any additional setup after loading the view, typically from a nib.
    [self initNetworkCommunication];
    [self sendMessage];
    
}

-(void)dealloc{
    [self stopTimer];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self initPlot];
    
    
    if (!_timer) {
        _timer = [NSTimer scheduledTimerWithTimeInterval:1.0f
                                                  target:self
                                                selector:@selector(_timerFired:)
                                                userInfo:nil repeats:YES];
    }
}

- (void)stopTimer {
    if ([_timer isValid]) {
        [_timer invalidate];
    }
    _timer = nil;
}

- (void)_timerFired:(NSTimer *)timer {
    NSLog(@"ping");
    
    if (infoqueue.count>= PLOT_POINTS) {
        NSLog(@"queue filled");
        [graph reloadData];
    }
    NSArray* que=[[StockStore sharedInstance] queuedPortfolioInfo];
    if (que!=nil) {
        if (que.count) {
            for (StockInfo *obj in que) {
                StockInfo *currentObj=[[[StockStore sharedInstance] getarray] lastObject];
                if (obj.purchasedQuantity!=nil) {
                    // need to buy
                    if ([currentObj.bestBuyQuantity intValue]>[obj.purchasedQuantity intValue] &&
                        [currentObj.bestBuyPrice intValue]<=[obj.userPrice intValue]) {
                        currentObj.purchasedQuantity=obj.purchasedQuantity;
                        [[StockStore sharedInstance] addPurchasedStockInfo:currentObj];
                        
                    }
                }else if(obj.soldQuantity!=nil){
                    // need to sell
                    if ([currentObj.bestSellQuantity intValue]>[obj.soldQuantity intValue] &&
                        [currentObj.bestSellPrice intValue]<=[obj.userPrice intValue]) {
                        currentObj.soldQuantity=obj.soldQuantity;
                        [[StockStore sharedInstance] addSoldStockInfo:currentObj];
                        
                    }
                }
                
            }
        }
    }
    
}

#pragma mark - Chart behavior
-(void)initPlot {
    [self configureGraph];
    [self configurePlots];
    [self configureAxes];
}

- (void)initNetworkCommunication {
    CFReadStreamRef readStream;
    CFWriteStreamRef writeStream;
    CFStreamCreatePairWithSocketToHost(NULL, (CFStringRef)@"0.0.0.0", 48129, &readStream, &writeStream);
    inputStream = (NSInputStream *)CFBridgingRelease(readStream);
    outputStream = (NSOutputStream *)CFBridgingRelease(writeStream);
    
    [inputStream setDelegate:self];
    [outputStream setDelegate:self];
    
    [inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    
    [inputStream open];
    [outputStream open];
    
}

- (void) sendMessage{
    // hello msg to recieve the content
    NSString *response  = [NSString stringWithFormat:@"%@",initmsg];
    NSData *data = [[NSData alloc] initWithData:[response dataUsingEncoding:NSASCIIStringEncoding]];
    [outputStream write:[data bytes] maxLength:[data length]];
}

- (void)stream:(NSStream *)theStream handleEvent:(NSStreamEvent)streamEvent {
    
    NSLog(@"stream event %lu", (unsigned long)streamEvent);
    
    switch (streamEvent) {
            
        case NSStreamEventOpenCompleted:
            NSLog(@"Stream opened");
            break;
        case NSStreamEventHasBytesAvailable:
            
            if (theStream == inputStream) {
                
                uint8_t buffer[1024];
                NSInteger len;
                
                while ([inputStream hasBytesAvailable]) {
                    len = [inputStream read:buffer maxLength:sizeof(buffer)];
                    if (len > 0) {
                        
                        NSString *output = [[NSString alloc] initWithBytes:buffer length:len encoding:NSASCIIStringEncoding];
                        
                        if (nil != output) {
                            
                            NSLog(@"server said: %@", output);
                            [self messageReceived:output];
                            
                        }
                    }
                }
            }
            break;
            
            
        case NSStreamEventErrorOccurred:
            
            NSLog(@"Can not connect to the host!");
            break;
            
        case NSStreamEventEndEncountered:
            
            [theStream close];
            [theStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
            theStream = nil;
            
            break;
        default:
            NSLog(@"Unknown event");
    }
    
}

- (void) messageReceived:(NSString *)message {
    
    StockInfo *newInfo=[[StockInfo alloc] init];
    NSArray *strings = [message componentsSeparatedByString:@","];
    if ([strings count]) {
        for (int i=0; i<6; i++) {
            NSString *str=strings[i];
            switch (i) {
                case 0:
                    newInfo.timeStamp=str;
//                    NSLog(@"time: %@",newInfo.timeStamp);
                    break;
                case 1:
                    newInfo.lastTradePrice=str;
//                    NSLog(@"ltp: %@",newInfo.lastTradePrice);
                    break;
                case 2:
                    newInfo.bestBuyPrice=str;
//                    NSLog(@"bbp: %@",newInfo.bestBuyPrice);
                    break;
                case 3:
                    newInfo.bestBuyQuantity=str;
//                    NSLog(@"bbq: %@",newInfo.bestBuyQuantity);
                    break;
                case 4:
                    newInfo.bestSellPrice=str;
//                    NSLog(@"bsp: %@",newInfo.bestSellPrice);
                    break;
                case 5:
                    newInfo.bestSellQuantity=str;
//                    NSLog(@"bsq: %@",newInfo.bestSellQuantity);
                    break;
                default:
                    break;
            }
        }
            NSLog(@"Obj: %@",newInfo);
        [[StockStore sharedInstance] addNewStockInfo:newInfo];
        [infoqueue enqueue:newInfo];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark- chart related
/* extern NSString *const kCPTDarkGradientTheme;
 extern NSString *const kCPTPlainBlackTheme;
 extern NSString *const kCPTPlainWhiteTheme;
 extern NSString *const kCPTSlateTheme;
 extern NSString *const kCPTStocksTheme;
 */

-(void)configureGraph {
    // 1 - Create the graph
    graph = [[CPTXYGraph alloc] initWithFrame:self.chartview.bounds];
    [graph applyTheme:[CPTTheme themeNamed:kCPTStocksTheme]];
    self.chartview.hostedGraph = graph;
    // 2 - Set graph title
    
    NSDate *newDate;
    NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:~ NSTimeZoneCalendarUnit fromDate:[NSDate date]];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"MMM dd, yyyy"];
    
    newDate = [[NSCalendar currentCalendar] dateFromComponents:dateComponents];
    NSLog(@"newDate: %@", [formatter stringFromDate:newDate]);
    
    NSString *title = [formatter stringFromDate:newDate];
    graph.title = title;
    // 3 - Create and set text style
    CPTMutableTextStyle *titleStyle = [CPTMutableTextStyle textStyle];
    titleStyle.color = [CPTColor whiteColor];
    titleStyle.fontName = @"Helvetica-Bold";
    titleStyle.fontSize = 16.0f;
    graph.titleTextStyle = titleStyle;
    graph.titlePlotAreaFrameAnchor = CPTRectAnchorTop;
    graph.titleDisplacement = CGPointMake(0.0f, 15.0f);
    // 4 - Set padding for plot area
    [graph.plotAreaFrame setPaddingLeft:40.0f];
    [graph.plotAreaFrame setPaddingBottom:-100.0f];
    // 5 - Enable user interactions for plot space
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *) graph.defaultPlotSpace;
    plotSpace.allowsUserInteraction = YES;
}

-(void)configurePlots {
    // 1 - Get graph and plot space
    CPTGraph *hostedgraph = self.chartview.hostedGraph;
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *) hostedgraph.defaultPlotSpace;
    
    CPTScatterPlot *googPlot = [[CPTScatterPlot alloc] init];
    googPlot.dataSource = self;
    googPlot.identifier = CPDTickerSymbolBBP;
    
    CPTColor *googColor = [CPTColor greenColor];
    [hostedgraph addPlot:googPlot toPlotSpace:plotSpace];
    
    // 3 - Set up plot space
    [plotSpace scaleToFitPlots:[NSArray arrayWithObjects: googPlot, nil]];
    CPTMutablePlotRange *xRange = [plotSpace.xRange mutableCopy];
    [xRange expandRangeByFactor:CPTDecimalFromCGFloat(1.0f)];
    plotSpace.xRange = xRange;
    CPTMutablePlotRange *yRange = [plotSpace.yRange mutableCopy];
    [yRange expandRangeByFactor:CPTDecimalFromCGFloat(100.0f)];
    plotSpace.yRange = yRange;
    
    
    CPTMutableLineStyle *googLineStyle = [googPlot.dataLineStyle mutableCopy];
    googLineStyle.lineWidth = 1.0;
    googLineStyle.lineColor = googColor;
    googPlot.dataLineStyle = googLineStyle;
    CPTMutableLineStyle *googSymbolLineStyle = [CPTMutableLineStyle lineStyle];
    googSymbolLineStyle.lineColor = googColor;
    CPTPlotSymbol *googSymbol = [CPTPlotSymbol trianglePlotSymbol];
    googSymbol.fill = [CPTFill fillWithColor:googColor];
    googSymbol.lineStyle = googSymbolLineStyle;
    googSymbol.size = CGSizeMake(6.0f, 6.0f);
    googPlot.plotSymbol = googSymbol;
    
}

-(void)configureAxes {
    // 1 - Create styles
    CPTMutableTextStyle *axisTitleStyle = [CPTMutableTextStyle textStyle];
    axisTitleStyle.color = [CPTColor whiteColor];
    axisTitleStyle.fontName = @"Helvetica-Bold";
    axisTitleStyle.fontSize = 12.0f;
    CPTMutableLineStyle *axisLineStyle = [CPTMutableLineStyle lineStyle];
    axisLineStyle.lineWidth = 2.0f;
    axisLineStyle.lineColor = [CPTColor whiteColor];
    CPTMutableTextStyle *axisTextStyle = [[CPTMutableTextStyle alloc] init];
    axisTextStyle.color = [CPTColor whiteColor];
    axisTextStyle.fontName = @"Helvetica-Bold";
    axisTextStyle.fontSize = 11.0f;
    CPTMutableLineStyle *tickLineStyle = [CPTMutableLineStyle lineStyle];
    tickLineStyle.lineColor = [CPTColor whiteColor];
    tickLineStyle.lineWidth = 2.0f;
    CPTMutableLineStyle *gridLineStyle = [CPTMutableLineStyle lineStyle];
    tickLineStyle.lineColor = [CPTColor blackColor];
    tickLineStyle.lineWidth = 1.0f;
    
    
    // 2 - Get axis set
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *) self.chartview.hostedGraph.axisSet;
    
    
    // 3 - Configure x-axis
    CPTAxis *x = axisSet.xAxis;
    x.title = @"Time";
    x.titleTextStyle = axisTitleStyle;
    x.titleOffset = 15.0f;
    x.axisLineStyle = axisLineStyle;
    x.labelingPolicy = CPTAxisLabelingPolicyNone;
    x.labelTextStyle = axisTextStyle;
    x.majorTickLineStyle = axisLineStyle;
    x.majorTickLength = 5.0f;
    x.tickDirection = CPTSignNegative;
    
    //    CGFloat dateCount = [[[CPDStockPriceStore sharedInstance] datesInMonth] count];
    CGFloat secCount = 5;
    NSMutableSet *xLabels = [NSMutableSet setWithCapacity:secCount];
    NSMutableSet *xLocations = [NSMutableSet setWithCapacity:secCount];
    NSInteger i = 0;
    
    for (int index=0; index<secCount; index++) {
        CPTAxisLabel *label = [[CPTAxisLabel alloc] initWithText:[NSString stringWithFormat:@"%d",index]  textStyle:x.labelTextStyle];
        CGFloat location = i++;
        label.tickLocation = CPTDecimalFromCGFloat(location);
        label.offset = x.majorTickLength;
        if (label) {
            [xLabels addObject:label];
            [xLocations addObject:[NSNumber numberWithFloat:location]];
        }
    }
    
    
    x.axisLabels = xLabels;
    x.majorTickLocations = xLocations;
    
    
    // 4 - Configure y-axis
    CPTAxis *y = axisSet.yAxis;
    y.title = @"Price";
    y.titleTextStyle = axisTitleStyle;
    y.titleOffset = -40.0f;
    y.axisLineStyle = axisLineStyle;
    y.majorGridLineStyle = gridLineStyle;
    y.labelingPolicy = CPTAxisLabelingPolicyNone;
    y.labelTextStyle = axisTextStyle;
    y.labelOffset = 10.0f;
    y.majorTickLineStyle = axisLineStyle;
    y.majorTickLength = 4.0f;
    y.minorTickLength = 2.0f;
    y.tickDirection = CPTSignPositive;
    NSInteger majorIncrement = 100;
    NSInteger minorIncrement = 50;
    
    CGFloat yMax = 5000.0f;  // should determine dynamically based on max price
    NSMutableSet *yLabels = [NSMutableSet set];
    NSMutableSet *yMajorLocations = [NSMutableSet set];
    NSMutableSet *yMinorLocations = [NSMutableSet set];
    for (NSInteger j = minorIncrement; j <= yMax; j += minorIncrement) {
        NSUInteger mod = j % majorIncrement;
        if (mod == 0) {
            CPTAxisLabel *label = [[CPTAxisLabel alloc] initWithText:[NSString stringWithFormat:@"%li", (long)(j/50)] textStyle:y.labelTextStyle];
            NSDecimal location = CPTDecimalFromInteger(j/50);
            label.tickLocation = location;
            label.offset = -y.majorTickLength - y.labelOffset;
            if (label) {
                [yLabels addObject:label];
            }
            [yMajorLocations addObject:[NSDecimalNumber decimalNumberWithDecimal:location]];
        } else {
            NSLog(@"j:%lu",j/50);
            [yMinorLocations addObject:[NSDecimalNumber decimalNumberWithDecimal:CPTDecimalFromInteger(j/50)]];
        }
    }
    y.axisLabels = yLabels;
    y.majorTickLocations = yMajorLocations;
    y.minorTickLocations = yMinorLocations;
    
}

#pragma mark - CPTPlotDataSource methods
-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot {
    return PLOT_POINTS;
}

-(NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index {
    NSInteger valueCount = PLOT_POINTS; // secs count
    
    switch (fieldEnum) {
        case CPTScatterPlotFieldX:
            if (index < valueCount) {
                return [NSNumber numberWithUnsignedInteger:index];
            }
            break;
            
        case CPTScatterPlotFieldY:
            if ([plot.identifier isEqual:CPDTickerSymbolBBP] == YES) {
                StockInfo *qInfo=[[StockInfo alloc] init];
                if(counter==PLOT_POINTS){
                    counter=0;
                }
                if (infoqueue.count>counter) {
                    qInfo=[infoqueue objectAtIndex:counter];
                    double value=[qInfo.bestBuyPrice doubleValue];
                    double yvalue=value;
                    NSLog(@"value:%f, yvalue:%f",value,yvalue);
                    counter++;
                    return [NSNumber numberWithDouble:yvalue];
                    
                }
            }
            break;
    }
    return [NSDecimalNumber zero];
}

@end
