//
//  SellStockController.m
//  TwoRoads
//
//  Created by KARTHIK B S on 10/04/16.
//  Copyright © 2016 KARTHIK B S. All rights reserved.
//

#import "SellStockController.h"
#import "StockStore.h"
#import "StockInfo.h"

@implementation SellStockController


@synthesize marketQuantitySell,limitQuantityPrice,limitQuantitySell,confirmLimitSellOutletBtn,confirmMarketSellOutletBtn;

-(void)viewDidLoad{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"stockblue.jpg"]]];
    
    confirmLimitSellOutletBtn.layer.masksToBounds=YES;
    confirmLimitSellOutletBtn.layer.cornerRadius=4;
    
    confirmMarketSellOutletBtn.layer.masksToBounds=YES;
    confirmMarketSellOutletBtn.layer.cornerRadius=4;
    
    
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //set initial values here
}


- (IBAction)marketConfirmBtn:(id)sender {
    NSMutableArray *array=[[StockStore sharedInstance] getarray];
    StockInfo *latestInfo=(StockInfo *) [array lastObject];
    int userQuant= [marketQuantitySell.text intValue];
    int availQnant=[latestInfo.bestSellQuantity intValue];
    if (availQnant>userQuant) {
        // buy the userQuant stocks
        latestInfo.soldQuantity=[NSString stringWithFormat:@"%d",userQuant];
        [[StockStore sharedInstance] addSoldStockInfo:latestInfo];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)limitConfirmBtn:(id)sender {
    NSMutableArray *array=[[StockStore sharedInstance] getarray];
    StockInfo *latestInfo=(StockInfo *) [array lastObject];
    int userQuant= [limitQuantitySell.text intValue];
    int availQnant=[latestInfo.bestSellQuantity intValue];
    int userPrice= [limitQuantityPrice.text intValue];
    int availPrice=[latestInfo.bestSellPrice intValue];
    
    latestInfo.soldQuantity=[NSString stringWithFormat:@"%d",userQuant];
    latestInfo.userPrice=limitQuantityPrice.text;
    if (availQnant>userQuant && availPrice<=userPrice) {
        // buy the userQuant stocks
        [[StockStore sharedInstance] addPurchasedStockInfo:latestInfo];
    }else{
        [[StockStore sharedInstance] addToQ:latestInfo];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [marketQuantitySell resignFirstResponder];
    [limitQuantitySell resignFirstResponder];
    [limitQuantityPrice resignFirstResponder];
}

@end
