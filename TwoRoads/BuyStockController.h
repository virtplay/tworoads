//
//  BuyStockController.h
//  TwoRoads
//
//  Created by KARTHIK B S on 10/04/16.
//  Copyright © 2016 KARTHIK B S. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BuyStockController : UIViewController<UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *marketConfirmOutletBtn;
@property (weak, nonatomic) IBOutlet UIButton *limitConfirmOutletBtn;

- (IBAction)marketConfirmBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *marketQuantityPurchase;

@property (weak, nonatomic) IBOutlet UITextField *limitQuantityPurchase;
@property (weak, nonatomic) IBOutlet UITextField *limitQuantityPrice;
- (IBAction)limitConfirmBtn:(id)sender;


@end
