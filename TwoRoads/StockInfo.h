//
//  StockInfo.h
//  TwoRoads
//
//  Created by KARTHIK B S on 09/04/16.
//  Copyright © 2016 KARTHIK B S. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StockInfo : NSObject

@property NSString *timeStamp;
@property NSString *lastTradePrice;
@property NSString *bestBuyPrice;
@property NSString *bestBuyQuantity;
@property NSString *bestSellPrice;
@property NSString *bestSellQuantity;

@property NSString *purchasedQuantity;
@property NSString *soldQuantity;

@property NSString *userPrice;

@end
