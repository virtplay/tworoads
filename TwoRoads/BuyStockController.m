//
//  BuyStockController.m
//  TwoRoads
//
//  Created by KARTHIK B S on 10/04/16.
//  Copyright © 2016 KARTHIK B S. All rights reserved.
//

#import "BuyStockController.h"
#import "StockStore.h"
#import "StockInfo.h"

@implementation BuyStockController

@synthesize marketQuantityPurchase,limitQuantityPrice,limitQuantityPurchase,marketConfirmOutletBtn,limitConfirmOutletBtn;


-(void)viewDidLoad{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"stockblue.jpg"]]];
    
    marketConfirmOutletBtn.layer.masksToBounds=YES;
    marketConfirmOutletBtn.layer.cornerRadius=4;
    
    limitConfirmOutletBtn.layer.masksToBounds=YES;
    limitConfirmOutletBtn.layer.cornerRadius=4;
    
    
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //set initial values here
}

- (IBAction)marketConfirmBtn:(id)sender {
    NSMutableArray *array=[[StockStore sharedInstance] getarray];
    StockInfo *latestInfo=(StockInfo *) [array lastObject];
    int userQuant= [marketQuantityPurchase.text intValue];
    int availQnant=[latestInfo.bestBuyQuantity intValue];
    if (availQnant>userQuant) {
        // buy the userQuant stocks
        latestInfo.purchasedQuantity=[NSString stringWithFormat:@"%d",userQuant];
        [[StockStore sharedInstance] addPurchasedStockInfo:latestInfo];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)limitConfirmBtn:(id)sender {
    
    NSMutableArray *array=[[StockStore sharedInstance] getarray];
    StockInfo *latestInfo=(StockInfo *) [array lastObject];
    int userQuant= [limitQuantityPurchase.text intValue];
    int availQnant=[latestInfo.bestBuyQuantity intValue];
    int userPrice= [limitQuantityPrice.text intValue];
    int availPrice=[latestInfo.bestBuyPrice intValue];
    
    latestInfo.purchasedQuantity=[NSString stringWithFormat:@"%d",userQuant];
    latestInfo.userPrice=limitQuantityPrice.text;
    if (availQnant>userQuant && availPrice<=userPrice) {
        // buy the userQuant stocks
        [[StockStore sharedInstance] addPurchasedStockInfo:latestInfo];
    }else{
        [[StockStore sharedInstance] addToQ:latestInfo];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [marketQuantityPurchase resignFirstResponder];
    [limitQuantityPurchase resignFirstResponder];
    [limitQuantityPrice resignFirstResponder];
}
@end
