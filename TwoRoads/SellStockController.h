//
//  SellStockController.h
//  TwoRoads
//
//  Created by KARTHIK B S on 10/04/16.
//  Copyright © 2016 KARTHIK B S. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SellStockController : UIViewController<UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *confirmMarketSellOutletBtn;

- (IBAction)marketConfirmBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *confirmLimitSellOutletBtn;
@property (weak, nonatomic) IBOutlet UITextField *marketQuantitySell;

@property (weak, nonatomic) IBOutlet UITextField *limitQuantitySell;
@property (weak, nonatomic) IBOutlet UITextField *limitQuantityPrice;
- (IBAction)limitConfirmBtn:(id)sender;


@end
