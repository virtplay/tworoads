//
//  HistroyViewController.m
//  TwoRoads
//
//  Created by KARTHIK B S on 10/04/16.
//  Copyright © 2016 KARTHIK B S. All rights reserved.
//

#import "HistroyViewController.h"
#import "StockStore.h"
#import "StockInfo.h"

@interface HistroyViewController ()
@property NSMutableArray *boughtarray;
@property NSMutableArray *soldarray;
@property NSMutableArray *totalarray;

@end

@implementation HistroyViewController
@synthesize boughtarray,soldarray,totalarray;

-(void)viewDidLoad{
    [super viewDidLoad];
    
    self.tableView.estimatedRowHeight = 45.0;
    self.tableView.rowHeight=UITableViewAutomaticDimension;
    
}

-(void)viewDidAppear:(BOOL)animated{
    boughtarray=[[[StockStore sharedInstance] BoughtPortfolioInfo] mutableCopy];
    soldarray=[[[StockStore sharedInstance] soldPortfolioInfo] mutableCopy];
    
    totalarray=[[boughtarray arrayByAddingObjectsFromArray:soldarray] mutableCopy];
    
    [self.tableView reloadData];
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return totalarray.count;    //count number of row from counting array hear cataGorry is An Array
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"MyIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:MyIdentifier] ;
    }
    
    StockInfo *obj=[totalarray objectAtIndex:indexPath.row];
    NSMutableString *dispStr=[[NSMutableString alloc] init];
    if (obj.purchasedQuantity!=nil) {
        // bought item
        NSDate* date = [NSDate dateWithTimeIntervalSince1970:[obj.timeStamp longLongValue]];
        dispStr=[[NSString stringWithFormat:@"Bought %@ stock/s for %@ price on %@",obj.purchasedQuantity,obj.bestBuyPrice,date] mutableCopy];
    }else if(obj.soldQuantity!=nil){
        // sold item
        NSDate* date = [NSDate dateWithTimeIntervalSince1970:[obj.timeStamp longLongValue]];
        dispStr=[[NSString stringWithFormat:@"Sold %@ stock/s for %@ price on %@",obj.soldQuantity,obj.bestSellPrice,date] mutableCopy];
    }
    
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.text = dispStr;
    return cell;
}

@end
