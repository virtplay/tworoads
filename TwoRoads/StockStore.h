//
//  StockStore.h
//  TwoRoads
//
//  Created by KARTHIK B S on 09/04/16.
//  Copyright © 2016 KARTHIK B S. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StockInfo.h"

@interface StockStore : NSObject

- (NSArray *)BoughtPortfolioInfo;
-(void) addPurchasedStockInfo:(StockInfo *) infoObj;

- (NSArray *)soldPortfolioInfo;
-(void) addSoldStockInfo:(StockInfo *) infoObj;

- (NSArray *)queuedPortfolioInfo;
-(void) addToQ:(StockInfo *) infoObj;

-(void) addNewStockInfo:(StockInfo *) infoObj;
-(NSMutableArray *) getarray;

+ (StockStore *)sharedInstance;

@end
