//
//  main.m
//  TwoRoads
//
//  Created by KARTHIK B S on 09/04/16.
//  Copyright © 2016 KARTHIK B S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
